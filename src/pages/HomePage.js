
import React, { useState } from "react";
import { Container, Row, Col } from 'react-grid-system';
import {
    InputGroup,
    InputGroupButtonDropdown, DropdownItem, FormGroup, Label, Input, DropdownToggle, DropdownMenu,
    ButtonToggle
} from 'reactstrap';
//import ReactDatetime from "react-datetime";
import * as utils from '../services/axios-functions';
import "moment/locale/es";
function HomePage(props) {

    const [documentSelect, setDocumentSelect] = useState({ id: 0, descripcion: "Seleccione documento" });
    const [documents, setDocuments] = useState([
        { id: 1, descripcion: "DNI" },
        { id: 2, descripcion: "Carnet de extranjeria" }
    ]);

    const [checkP, setCheckP] = useState(false);
    const [checkE, setCheckE] = useState(false);

    const [personSelect, setPersonSelect] = useState([
        { n_document: '', first_name: '', last_name: '', last_name_2: '', birthdate: '', phone: '', gender: '' }
    ]);

    const [dropdownOpen, setDropdownOpen] = useState(false);

    const toggleDropDown = () => setDropdownOpen(!dropdownOpen);

    React.useEffect(() => {
        const functions_first = async () => {
            //await loadData();
        }
        functions_first();
    }, []);

    const buscar = async () => {

        if (documentSelect.id == 0) {
            alert("Seleccione un tipo de documento");
        } else if (!personSelect.n_document) {
            alert("Ingrese numero de documento");
        } else if (!personSelect.birthdate) {
            alert("Seleccione su fecha de cumpleaños");
        } else if (!personSelect.phone) {
            alert("Ingrese un numero de telefono");
        } else if (!checkE || !checkP) {
            alert("Debe de aceptar las politicas");
        } else {
            let document = personSelect.n_document;
            let response = await utils.getUsers(document);
            props.history.push(`/step-1/${response[0].n_document}`);
        }
    }

    return (
        <>
            <Container fluid>
                <Row >
                    <Col md={7} sm={12} lg={7}>1 of 2</Col>
                    <Col md={7} sm={12} lg={5}>
                        <br />
                        <h3 className="center">Obten tu <span className="text-red"> seguro ahora</span></h3>
                        <br />
                        <h5 className="center" >Ingresa los datos para comenzar</h5>
                        <br />
                        <Row>
                            <Col md={12} sm={12} lg={12}>
                                <InputGroup>
                                    <InputGroupButtonDropdown addonType="append" isOpen={dropdownOpen} toggle={toggleDropDown}>
                                        <DropdownToggle caret>
                                            {documentSelect.descripcion}
                                        </DropdownToggle>
                                        <DropdownMenu>
                                            {
                                                documents &&
                                                documents.map(item =>
                                                (
                                                    <DropdownItem key={item.id} onClick={() => setDocumentSelect({ ...documentSelect, id: item.id, descripcion: item.descripcion })}>{item.descripcion}</DropdownItem >
                                                ))
                                            }
                                        </DropdownMenu>
                                    </InputGroupButtonDropdown>
                                    <Input type="number"
                                        placeholder="Ingrese su numero de documento"
                                        onChange={(e) => setPersonSelect({ ...personSelect, n_document: e.target.value })} />
                                </InputGroup>
                            </Col>
                        </Row>
                        <br />
                        <Row>
                            <Col md={12} sm={12} lg={12}>
                                <Label for="date">Date</Label>
                                <InputGroup >
                                    <Input
                                        type="date"
                                        name="date"
                                        id="date"
                                        placeholder="date placeholder"
                                        onChange={(e) => setPersonSelect({ ...personSelect, birthdate: e.target.value })}
                                        value={personSelect.birthdate}
                                    />
                                </InputGroup>
                            </Col>
                        </Row>
                        <br />
                        <Row>
                            <Col md={12} sm={12} lg={12}>
                                <Label for="phone">Celular</Label>
                                <InputGroup >
                                    <Input
                                        id="phone"
                                        type="number"
                                        placeholder="Ingresa tu numero de celular"
                                        onChange={(e) => setPersonSelect({ ...personSelect, phone: e.target.value })}
                                    />
                                </InputGroup>
                            </Col>
                        </Row>
                        <br />
                        <Row>
                            <Col md={12} sm={12} lg={12}>
                                <FormGroup check>
                                    <Label check>
                                        <Input type="checkbox" checked={checkP} onChange={() => setCheckP(!checkP)} /> Acepto la Política de Protección de Datos Personales y los Términos y Condiciones.
                                    </Label>
                                </FormGroup>
                            </Col>
                        </Row>
                        <br />
                        <Row>
                            <Col md={12} sm={12} lg={12}>
                                <FormGroup check>
                                    <Label check>
                                        <Input type="checkbox" checked={checkE} onChange={() => setCheckE(!checkE)} /> Acepto la Política de Envío de Comunicaciones Comerciales.
                                    </Label>
                                </FormGroup>
                            </Col>
                        </Row>
                        <br />
                        <Row>
                            <Col md={12} sm={12} lg={12} className="center-component">
                                <ButtonToggle color="danger" onClick={() => buscar()}>Comencemos</ButtonToggle>
                            </Col>
                        </Row>
                    </Col>
                </Row>
            </Container>
        </>
    );
}

export default HomePage;