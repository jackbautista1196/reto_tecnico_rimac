//API LINK
const url_api = 'https://randomuser.me/api';
const person = [
    { n_document: '70358111', first_name: 'Jack', last_name: 'Bautista', last_name_2: 'Castilla', birthdate: '11-06-1996', phone: '987654321', gender: 'male' },
    { n_document: '75723268', first_name: 'Nataly', last_name: 'Laos', last_name_2: 'Rojas', birthdate: '02-08-1996', phone: '987654321', gender: 'female' },
    { n_document: '70358112', first_name: 'Jhan', last_name: 'Bautista', last_name_2: 'Castilla', birthdate: '30-03-2002', phone: '987654321', gender: 'female' }
];
module.exports = {
    url_api: url_api,
    person: person
};